# ocean_cheatsheet

Uses wiki to create a cheat sheet for the physical oceanography.

Wiki uses markdown to render text and [KaTeX](https://katex.org/docs/supported.html) to render math equations.